﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace SelectionSort
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch timer = new Stopwatch();
            Console.WriteLine("Please enter number to be sorted seperating each number by a comma:");
            string[] input = (Console.ReadLine()).Split(',');
            timer.Start();

            int[] unsorted = new int[input.Length];

            for (int i = 0; i < input.Length; i++)
            {
                unsorted[i] = Convert.ToInt32(input[i]);
            }


            sort(unsorted);

            Console.WriteLine("\n\nThe Sorted List is: ");
            foreach (int num in unsorted)
            {
                Console.Write(num + "\t");
            }
            timer.Stop();
            Console.WriteLine("\nTime Taken: {0}", timer.Elapsed);
            Console.WriteLine("\n\nPress any key to continue . . . ");
            Console.ReadKey();
        }
        public static int[] sort(int[] a)
        {
            for(int i = a.Length-1; i > 0; i--)
            {
                int positionOfMax = 0;
                for (int j = 0; j <= i; j++)
                {
                    if (a[j] > a[positionOfMax])
                    {
                        positionOfMax = j;
                    }
                }
                int temp = a[i];
                a[i] = a[positionOfMax];
                a[positionOfMax] = temp;
            }

            return a;
        }
    }
}
